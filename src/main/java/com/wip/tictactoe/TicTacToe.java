/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.tictactoe;

import java.util.Scanner;

/**
 *
 * @author WIP
 */
public class TicTacToe {

    static Scanner kb = new Scanner(System.in);
    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();

        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            processGame();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn" + currentPlayer);
    }

    private static void inputRowCol() {
        System.out.println("Please input row, col:");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    private static void processGame() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showTable();
                System.out.println(">>>" + currentPlayer + " Win<<<");
                return;
            }
            if (checkDraw()) {
                showDraw();
                finish = true;
            }
            count++;
            switchTurn();
        }

    }

    private static void switchTurn() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    private static boolean setTable() {
        if (table[row - 1][col - 1] != 'O' && table[row - 1][col - 1] != 'X') {     
            table[row - 1][col - 1] = currentPlayer;
            return true;
        } else {
            System.out.println("this place is duplicate, please input another row, col");
            repeatTurn();
            count--;
            return false;
        }
    }

    private static boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() { // 11, 22, 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() { // 13, 22, 31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        if (count != 8) {
            return false;
        }
        return true;
    }

    private static void showDraw() {
        System.out.println(">>> Draw <<<");
    }

    private static void repeatTurn() {
        if (currentPlayer == 'O') {
            currentPlayer = 'O';
        } else if(currentPlayer == 'X') {
            currentPlayer = 'X';
        }
    }

}
